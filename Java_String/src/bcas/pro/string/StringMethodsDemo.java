package bcas.pro.string;
public class StringMethodsDemo {
	public static void main(String[] args) {
		StringMethods demo = new StringMethods();
		String name1 = "Jamestalan Judo";
		String name2 = "Konsikan";
		String name3 = "Vasantharaj";
		String name4 = "Mathiyalagan";
		String name5 = "   Silaxan   ";

		demo.CharAt(name1);
		demo.Concat(name1, name2);
		demo.IndexOf1(name1);
		demo.IndexOf2(name1);
		demo.IndexOf3(name1, name2);
		demo.IndexOf4(name3, name4);
		demo.LastIndexOf1(name2);
		demo.LastIndexOf2(name2);
		demo.LastIndexOf3(name3, name4);
		demo.LastIndexOf4(name2, name1);
		demo.Length(name2);
		demo.Replace(name2);
		demo.StartsWith(name1);
		demo.Substring1(name1);
		demo.Substring2(name1);
		demo.ToLowerCase(name1);
		demo.ToUpperCase(name2);
		demo.Trim(name5);
	}
}

/*
OUTPUT

...................................

TEST : 1 | String charAt() Method |

INPUT   : Jamestalan Judo
OUTPUT  : a

................................................

TEST : 5 | String concat() Method |

INPUT   : Jamestalan Judo,Konsikan
OUTPUT  : Jamestalan JudoKonsikan
.....................................

TEST : 16 | String indexOf() Method |

INPUT   : Jamestalan Judo
OUTPUT  : 14
...................................................

TEST : 17 | String indexOf(int ch, int fromIndex) |

INPUT   : Jamestalan Judo
OUTPUT  : -1
...............................................

TEST : 18 | String indexOf(String str) Method |

INPUT   : Jamestalan Judo,Konsikan
OUTPUT  : -1
.......................................................

TEST : 19 | String indexOf(String str, int fromIndex) |

INPUT   : Vasantharaj,Mathiyalagan
OUTPUT  : -1
.........................................

TEST : 21 | String lastIndexOf() Method |

INPUT   : Konsikan
OUTPUT  : 6
.......................................................

TEST : 22 | String lastIndexOf(int ch, int fromIndex) |

INPUT   : Konsikan
OUTPUT  : -1
...................................................

TEST : 23 | String lastIndexOf(String str) Method |

INPUT   : Vasantharaj,Mathiyalagan
OUTPUT  : -1
............................................

TEST : 24 | String lastIndexOf() fromIndex |

INPUT   : Konsikan,Jamestalan Judo
OUTPUT  : -1
....................................

TEST : 25 | String length() Method |

INPUT   : Konsikan
OUTPUT  : 8
.....................................

TEST : 29 | String replace() Method |

INPUT   : Konsikan
OUTPUT  : KoXsikaX
........................................

TEST : 34 | String startsWith() Method |

INPUT   : Jamestalan Judo
OUTPUT  : true
.......................................

TEST : 37 | String substring() Method |

INPUT   : Jamestalan Judo
OUTPUT  : talan Judo
....................................................

TEST : 38 | String substring(beginIndex, endIndex) |

INPUT   : Jamestalan Judo
OUTPUT  : talan J
.........................................

TEST : 40 | String toLowerCase() Method |

INPUT   : Jamestalan Judo
OUTPUT  : jamestalan judo
.........................................

TEST : 43 | String toUpperCase() Method |

INPUT   : Konsikan
OUTPUT  : KONSIKAN
..................................

TEST : 45 | String trim() Method |

INPUT   :    Silaxan
OUTPUT  : Silaxan
*/