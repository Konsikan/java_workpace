package bcas.pro.string;
public class StringMethods {

	public void CharAt(String name1) {
		System.out.println("...................................");
		System.out.println();
		System.out.println("TEST : 1 | String charAt() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name1);
		char result = name1.charAt(8);
		System.out.println("OUTPUT \t: " + result);
	}

	public void Concat(String name1, String name2) {
		System.out.println();
		System.out.println("................................................");
		System.out.println();
		System.out.println("TEST : 5 | String concat() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name1 + "," + name2);
		System.out.println("OUTPUT \t: " + name1.concat(name2));
	}

	public void IndexOf1(String name1) {
		System.out.println(".....................................");
		System.out.println();
		System.out.println("TEST : 16 | String indexOf() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name1);
		System.out.println("OUTPUT \t: " + name1.indexOf("o"));
	}

	public void IndexOf2(String name1) {
		System.out.println("...................................................");
		System.out.println();
		System.out.println("TEST : 17 | String indexOf(int ch, int fromIndex) |");
		System.out.println();
		System.out.println("INPUT \t: " + name1);
		System.out.println("OUTPUT \t: " + name1.indexOf("a", 12));
	}

	public void IndexOf3(String name1, String name2) {
		System.out.println("...............................................");
		System.out.println();
		System.out.println("TEST : 18 | String indexOf(String str) Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name1 + "," + name2);
		System.out.println("OUTPUT \t: " + name1.indexOf(name2));
	}

	public void IndexOf4(String name3, String name4) {
		System.out.println(".......................................................");
		System.out.println();
		System.out.println("TEST : 19 | String indexOf(String str, int fromIndex) |");
		System.out.println();
		System.out.println("INPUT \t: " + name3 + "," + name4);
		System.out.println("OUTPUT \t: " + name3.indexOf(name4, 8));
	}

	public void LastIndexOf1(String name1) {
		System.out.println(".........................................");
		System.out.println();
		System.out.println("TEST : 21 | String lastIndexOf() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name1);
		System.out.println("OUTPUT \t: " + name1.lastIndexOf("a"));
	}

	public void LastIndexOf2(String name1) {
		System.out.println(".......................................................");
		System.out.println();
		System.out.println("TEST : 22 | String lastIndexOf(int ch, int fromIndex) |");
		System.out.println();
		System.out.println("INPUT \t: " + name1);
		System.out.println("OUTPUT \t: " + name1.lastIndexOf("k", 4));
	}

	public void LastIndexOf3(String name3, String name4) {
		System.out.println("...................................................");
		System.out.println();
		System.out.println("TEST : 23 | String lastIndexOf(String str) Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name3 + "," + name4);
		System.out.println("OUTPUT \t: " + name3.lastIndexOf(name4));
	}

	public void LastIndexOf4(String name2, String name1) {
		System.out.println("............................................");
		System.out.println();
		System.out.println("TEST : 24 | String lastIndexOf() fromIndex |");
		System.out.println();
		System.out.println("INPUT \t: " + name2 + "," + name1);
		System.out.println("OUTPUT \t: " + name2.lastIndexOf(name1, 5));
	}

	public void Length(String name2) {
		System.out.println("....................................");
		System.out.println();
		System.out.println("TEST : 25 | String length() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name2);
		System.out.println("OUTPUT \t: " + name2.length());
	}

	public void Replace(String name2) {
		System.out.println(".....................................");
		System.out.println();
		System.out.println("TEST : 29 | String replace() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name2);
		System.out.println("OUTPUT \t: " + name2.replace("n", "X"));
	}

	public void StartsWith(String name1) {
		System.out.println("........................................");
		System.out.println();
		System.out.println("TEST : 34 | String startsWith() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name1);
		System.out.println("OUTPUT \t: " + name1.startsWith("Jamestalan"));
	}

	public void Substring1(String name1) {
		System.out.println(".......................................");
		System.out.println();
		System.out.println("TEST : 37 | String substring() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name1);
		System.out.println("OUTPUT \t: " + name1.substring(5));
	}

	public void Substring2(String name1) {
		System.out.println("....................................................");
		System.out.println();
		System.out.println("TEST : 38 | String substring(beginIndex, endIndex) |");
		System.out.println();
		System.out.println("INPUT \t: " + name1);
		System.out.println("OUTPUT \t: " + name1.substring(5, 12));
	}

	public void ToLowerCase(String name1) {
		System.out.println(".........................................");
		System.out.println();
		System.out.println("TEST : 40 | String toLowerCase() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name1);
		System.out.println("OUTPUT \t: " + name1.toLowerCase());
	}

	public void ToUpperCase(String name2) {
		System.out.println(".........................................");
		System.out.println();
		System.out.println("TEST : 43 | String toUpperCase() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name2);
		System.out.println("OUTPUT \t: " + name2.toUpperCase());
	}

	public void Trim(String name5) {
		System.out.println("..................................");
		System.out.println();
		System.out.println("TEST : 45 | String trim() Method |");
		System.out.println();
		System.out.println("INPUT \t: " + name5);
		System.out.println("OUTPUT \t: " + name5.trim());
	}
}
