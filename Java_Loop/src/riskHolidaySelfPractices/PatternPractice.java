package riskHolidaySelfPractices;

public class PatternPractice {
	// QUESTION 1
	public void Q1(int num) {
		System.out.println("");
		System.out.println("|QUESTION1|");
		System.out.println("");
		int n = 1;
		do {
			for (int i = 0; i <= num; i++) {
				if (i == ((num / 2) - n)) {
					System.out.print("/");
				}
				if (i == ((num / 2) + n)) {
					System.out.print("\\");
				} else if (i != (num / 2)) {
					System.out.print(" ");
				} else {
					System.out.print("");
				}
			}
			System.out.println();
			n = n + 1;
		} while (n <= num / 2);
		int p = num / 2;
		do {
			for (int i = 0; i <= num; i++) {
				if (i == ((num / 2) - p)) {
					System.out.print("\\");
				}
				if (i == ((num / 2) + p - 1)) {
					System.out.print("/");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
			p = p - 1;
		} while (p > 0);
	}

	// QUESTION 3
	public void Q3(int num) {
		System.out.println("");
		System.out.println("|QUESTION2|");
		System.out.println("");
		int n = 1;
		do {
			for (int i = 0; i < num; i++) {
				if (i == n || i == (num - n)) {
					System.out.print("x");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println();
			n = n + 1;
		} while (n < num);
	}

	// QUESTION 4
	public void Q4(int num) {
		System.out.println("");
		System.out.println("      |QUESTION4|");
		System.out.println("");
		for (int t = 1; t <= num; t++) {
			if (t % 2 == 0) {
				for (int i = 1; i <= (num + 1); i++) {
					System.out.print(" /");
					if (i % (num + 1) == 0) {
						System.out.println(" ");
					}
				}
			} else {
				for (int i = 1; i <= (num + 2); i++) {
					System.out.print("/ ");
					if (i % (num + 2) == 0) {
						System.out.println(" ");
					}
				}
			}
		}
	}

	// QUESTION 4
	public void Q5(int num) {
		System.out.println("");
		System.out.println("      |QUESTION5|");
		System.out.println("");
		for (int t = 1; t <= num; t++) {
			if (t % 2 == 0) {
				for (int i = 1; i <= (num + 1); i++) {
					System.out.print(" \\");
					if (i % (num + 1) == 0) {
						System.out.println(" ");
					}
				}
			} else {
				for (int i = 1; i <= (num + 2); i++) {
					System.out.print("\\ ");
					if (i % (num + 2) == 0) {
						System.out.println(" ");
					}
				}
			}
		}
	}
}