package riskHolidaySelfPractices;

import riskHolidaySelfPractices.PatternPractice;

public class PatternPracticeDemo {
	public static void main(String args[]) {
		PatternPractice demo = new PatternPractice();
		int x = 10;
		demo.Q1(x);
		demo.Q3(x);
		demo.Q4(x);
		demo.Q5(x);
	}

}

/*

OUTPUT


|QUESTION1|

    / \
   /   \
  /     \
 /       \
/         \
\         /
 \       /
  \     /
   \   /
    \ /

|QUESTION2|

 x       x
  x     x
   x   x
    x x
     x
    x x
   x   x
  x     x
 x       x

      |QUESTION4|

/ / / / / / / / / / / /
 / / / / / / / / / / /
/ / / / / / / / / / / /
 / / / / / / / / / / /
/ / / / / / / / / / / /
 / / / / / / / / / / /
/ / / / / / / / / / / /
 / / / / / / / / / / /
/ / / / / / / / / / / /
 / / / / / / / / / / /

      |QUESTION5|

\ \ \ \ \ \ \ \ \ \ \ \
 \ \ \ \ \ \ \ \ \ \ \
\ \ \ \ \ \ \ \ \ \ \ \
 \ \ \ \ \ \ \ \ \ \ \
\ \ \ \ \ \ \ \ \ \ \ \
 \ \ \ \ \ \ \ \ \ \ \
\ \ \ \ \ \ \ \ \ \ \ \
 \ \ \ \ \ \ \ \ \ \ \
\ \ \ \ \ \ \ \ \ \ \ \
 \ \ \ \ \ \ \ \ \ \ \
 
 */
